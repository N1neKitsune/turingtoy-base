from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:
    # element de la machine
    blank = machine['blank']  # symbole cases vides
    current_state = machine['start state']  # etat initial 
    final_states = set(machine['final states'])  # etats finaux
    transition_table = machine['table']  # transition table
    
    # init bande
    tape = list(input_)
    tape_head = 0
    
    # log
    execution_log = []

    # save actual state in log
    def log_step(state, symbol, position, tape_state, transition):
        execution_log.append({
            "current_state": state,
            "current_symbol": symbol,
            "head_position": position,
            "tape_content": "".join(tape_state),
            "transition": transition
        })
    
    # exec turing
    executed_steps = 0
    while steps is None or executed_steps < steps:
        # sortie des limites
        if tape_head < 0:
            tape.insert(0, blank)
            tape_head = 0
        elif tape_head >= len(tape):
            tape.append(blank)
        
        current_symbol = tape[tape_head]  # symbole pointer
        if current_state in transition_table and current_symbol in transition_table[current_state]:
            transition = transition_table[current_state][current_symbol]
            log_step(current_state, current_symbol, tape_head, tape, transition)

            # transition
            if isinstance(transition, str):
                if transition == "R":
                    tape_head += 1
                elif transition == "L":
                    tape_head -= 1
            else:
                if 'write' in transition:
                    tape[tape_head] = transition['write']
                if 'R' in transition:
                    current_state = transition['R']
                    tape_head += 1
                elif 'L' in transition:
                    current_state = transition['L']
                    tape_head -= 1
            
            executed_steps += 1

            # etat final verif
            if current_state in final_states:
                break
        else:
            # If no transition turing block
            log_step(current_state, current_symbol, tape_head, tape, None)
            break
    
    # Clean
    final_output = ''.join(tape).strip(blank)
    is_halted = current_state in final_states
    return final_output, execution_log, is_halted
